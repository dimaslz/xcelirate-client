var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var HttpClient = /** @class */ (function () {
    function HttpClient(baseURL) {
        this.instance = HttpClient.create(baseURL, {
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }
    HttpClient.create = function (url, config) {
        var _this = this;
        return {
            post: function (path, _a) {
                if (path === void 0) { path = ''; }
                var _b = _a === void 0 ? {} : _a, data = _b.data;
                return fetch("" + url + path, __assign(__assign({}, config), { method: 'POST', body: JSON.stringify(data) }))
                    .then(_this._handleResponse)
                    .catch(_this._handleError);
            }
            // ... other methods
        };
    };
    HttpClient._handleResponse = function (data) {
        return data;
    };
    HttpClient._handleError = function (error) {
        return Promise.reject(error);
    };
    return HttpClient;
}());
export { HttpClient };
