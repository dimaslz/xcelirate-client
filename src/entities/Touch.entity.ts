export type Touch = {
	time: number;
	$event: TouchEvent;
}