export type Click = {
	time: number;
	$event: MouseEvent;
}