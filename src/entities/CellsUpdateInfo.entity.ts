import { Cell } from './Cell.entity';

export type CellsUpdateInfo = {
	all: Cell[];
	selected: Cell[];
	rest: Cell[];
}