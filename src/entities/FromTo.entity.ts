export type FromTo = {
	from: number;
	to: number;
}