export type Cell = {
  index: number;
  col: number;
  row: number;
  color: string;
  style: string;
}