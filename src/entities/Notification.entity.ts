export type Notification = {
	position?: string,
	type?: string,
	message: string;
}