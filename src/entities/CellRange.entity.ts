import { FromTo } from './FromTo.entity';

export type CellRange = {
	row: FromTo;
	col: FromTo;
}