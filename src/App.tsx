import React, { useEffect, useState } from 'react';

import './App.scss';

import { Board } from './components/Board';
import { Notification as NotificationComponent } from './components/Notification';
import EventBus from './utils/event-bus';
import { Notification } from './entities';

function App() {
	const [notification, setNotification] = useState<Notification>({
		message: ''
	});

	useEffect(() => {
		const busInstance = EventBus.subscribe('notification', (notification: Notification) => {
			setNotification(notification);
		});

		return () => busInstance.unsubscribe();
	}, []);

	return (
		<div
			className="App"
		>
			<Board
				size={5}
			/>

			{notification.message && <NotificationComponent notification={notification} />}
		</div>
	);
}

export default App;
