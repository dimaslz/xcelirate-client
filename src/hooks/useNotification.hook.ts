import { useState, useEffect } from 'react';
import { Notification } from '../entities';
import EventBus from '../utils/event-bus';

export function useNotification(delay: number = 2000) {
	const [notification, setNotification] = useState<Notification>({
		message: '',
		type: 'success'
	});

	useEffect(
		() => {
			let tOInstance: any;
			if (notification.message) {
				tOInstance = setTimeout(() => {
					setNotification({ message: '' });
				}, delay);
			}

			EventBus.publish('notification', notification);

			return () => {
				clearTimeout(tOInstance);
			};

		}, [notification.message]);

	return setNotification;
}