import React from 'react';

import './Notification.component.scss';

import { Notification } from '../../entities';

/**
 * Notification component. Can be appear on top or bottom of the screen
 * Just is showed if the message property is not empty
 *
 * @param props { notification: Notification }
 * @returns
 */
function NotificationComponent({ notification }: { notification: Notification}) {

	const { type = 'success', position = 'top', message } = notification;

	return (<div
		className={[`Notification Notification__${type}`].join(' ')}
		style={{ [`${position}`]: 0 }}
	        >
		{ message }
	</div>);
}

export default NotificationComponent;