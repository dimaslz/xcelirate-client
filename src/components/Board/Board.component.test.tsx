import '@testing-library/jest-dom/extend-expect';
import React from 'react';
import { render, screen, act, fireEvent } from '@testing-library/react';

import Board from './Board.component';
import userEvent from '@testing-library/user-event';

describe('Board', () => {
	beforeEach(() => {
		jest.useFakeTimers();
	});

	afterEach(() => {
		jest.runOnlyPendingTimers();
		jest.useRealTimers();
	});

	describe('default states', () => {
		test('when the size is less than 2, should print a message', () => {
			const { getByText } = render(<Board size={0} />);

			const messageElement = getByText(/Please, size over 2/i);
			expect(messageElement).toBeInTheDocument();
		});

		test('by default should be a board of 5x5', () => {
			render(<Board size={5} />);

			const cells = document.querySelectorAll('.Cell');
			expect(cells.length).toBe(25);
		});
	});

	describe('actions', () => {
		describe('[OnClick] in a cell, should switch the color', () => {
			test('now the cell 0 should be red', () => {
				render(<Board size={5} />);

				const cell = screen.getByTestId('cell_0');
				userEvent.click(cell);
				act((): any => jest.advanceTimersByTime(300));

				expect(cell).toHaveClass('Cell__red');
			});
		});

		describe('[OnDoubleClick] in a cell, should switch the color of the column', () => {
			beforeEach(() => {
				render(<Board size={5} />);

				const cell = screen.getByTestId('cell_0');
				userEvent.click(cell);
				act((): any => jest.advanceTimersByTime(300));
				userEvent.dblClick(cell);
				act((): any => jest.advanceTimersByTime(300));
			});

			[0, 5, 10, 15, 20].forEach(cellIndex => {
				test(`now cell index ${cellIndex} should be red`, () => {
					const cell = screen.getByTestId(`cell_${cellIndex}`);
					expect(cell).toHaveClass('Cell__red');
				});
			});
		});

		describe('[OnSelect] a group of cells should highligh the following cells', () => {
			beforeEach(() => {
				render(<Board size={5} />);
				jest.useFakeTimers();

				const cell = screen.getByTestId('cell_0');
				userEvent.click(cell);
				act((): any => jest.advanceTimersByTime(310));

				fireEvent.mouseDown(cell);

				const cell06 = screen.getByTestId('cell_6');
				fireEvent.mouseEnter(cell06);
			});

			[0, 1, 5, 6].forEach(cellNumber => {
				test(`cell ${cellNumber} is highlighted`, () => {
					const cell = screen.getByTestId(`cell_${cellNumber}`);
					expect(cell).toHaveClass('highlight');
				});
			});

		});

		describe('[OnMouseIsUp], the select should switch the color of the following cells', () => {
			beforeEach(() => {
				render(<Board size={5} />);
				jest.useFakeTimers();

				const cell = screen.getByTestId('cell_0');
				userEvent.click(cell);
				act((): any => jest.advanceTimersByTime(300));

				fireEvent.mouseDown(cell);

				const cell06 = screen.getByTestId('cell_6');
				fireEvent.mouseEnter(cell06);
				fireEvent.mouseUp(cell06);
			});

			[0, 1, 5, 6].forEach(cellNumber => {
				test(`cell ${cellNumber} is red`, () => {
					const cell = screen.getByTestId(`cell_${cellNumber}`);
					expect(cell).toHaveClass('Cell__red');
				});
			});
		});
	});

});
