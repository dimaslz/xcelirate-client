/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import throttle from 'lodash.throttle';

import { Cell, CellRange, CellsUpdateInfo, Click, Touch } from '../../entities';
import Api from '../../@xcelirate-sdk';
import { useNotification } from '../../hooks/useNotification.hook';
import IS_MOBILE from '../../utils/is-mobile';

import {
	generateCellsData,
	toggleColor,
	applyColorToAGroupOfCells,
	getSelectedCoordinatesCells,
	getCellDataFromEvent,
	generateBoard,
	applyStyleToAGroupOfCells
} from './methods';
import { ReloadIcon } from '../ReloadIcon';

const TIME_OF_SINGLE_CLICK = 200; // time to consider that is a single click
const DEFAULT_SIZE = 5; // default size of the board

function Board(props: { size: number } = { size: DEFAULT_SIZE }) {
	const [size, setSize] = useState<number>(0);
	const [cells, setCells] = useState<Cell[]>([]);
	const [isMobile, setIsMobile] = useState<boolean>(false);
	const [sourceCell, setSourceCell] = useState<Cell | any>(null);

	// click state
	const [clickPressed, setClickPressed] = useState<boolean>(false);
	const [touchs, setTouchs] = useState<Touch[]>([]);
	const [touchTimeOutInstance, setTouchTimeOutInstance] = useState<any>(null);
	const [clickTimeOutInstance, setClickTimeOutInstance] = useState<any>(null);
	const [cellsToNotify, setCellsToNotify] = useState<Cell[]>([]);

	// custom hooks
	const useNotificationHook = useNotification();

	let notificationTimeout: any = null;
	function sendNotification() {
		notificationTimeout = setTimeout(async () => {
			try {
				await Api.track({
					cells: cellsToNotify
				});

				useNotificationHook({ message: 'Cells has been changed and tracked!' });
				setCellsToNotify([]);
			}
			catch (err) {
				useNotificationHook({ message: 'Some error has been occurred. Please, try again or check the network', type: 'error' });
			}

		}, 1000);
	}

	function clearClickTimeout(): void {
		clearTimeout(clickTimeOutInstance);
		setClickTimeOutInstance(0);
	}

	function clearTouchTimeout(): void {
		clearTimeout(touchTimeOutInstance);
		setTouchTimeOutInstance(0);
		setTouchs([]);
	}

	function applyColorToColumn(col: number, color: string): void {
		let cellsCopy: Cell[] = [...cells];

		let cellsChanged: Cell[] = [];
		cellsCopy = cellsCopy.map((cell: Cell) => {
			if (cell.col === col) {
				const newCell = {
					...cell,
					color
				};

				cellsChanged.push(newCell);
				return newCell;
			}
			return cell;
		});

		setCellsToNotify([...cellsToNotify, ...cellsChanged ]);

		setCells(cellsCopy);
	}

	function clickAction($event: MouseEvent) {
		return setTimeout(() => {
			const cell: Cell | null = getCellDataFromEvent($event, cells);
			if (cell) {
				const newCells: Cell[] = toggleColor([...cells], cell.index);
				setCells(newCells);

				setCellsToNotify([ ...cellsToNotify, cell ]);
			}

			setClickPressed(false);

			clearClickTimeout();
		}, TIME_OF_SINGLE_CLICK);
	}

	function onClickInCell($event: MouseEvent) {
		clearClickTimeout();
		if ($event.detail === 1) {
			setClickTimeOutInstance(clickAction($event));
		}

		if ($event.detail % 2 === 0) {
			onDoubleClickInCell($event);
		}
	}

	function onDoubleClickInCell($event: MouseEvent): void {
		const cell: Cell | null = getCellDataFromEvent($event, cells);

		if (cell) applyColorToColumn(cell.col, cell.color);
	}

	function onMouseDownInCell($event: MouseEvent): void {
		const cell: Cell | null = getCellDataFromEvent($event, cells);

		setClickPressed(true);
		if (cell) setSourceCell(cell);

	}

	function onMouseOverInCell($event: MouseEvent) {
		if (clickPressed && sourceCell) {
			const cell: Cell | null = getCellDataFromEvent($event, cells);
			if (!cell) return;

			const selectedGroup: CellRange = getSelectedCoordinatesCells({
				cell,
				sourceCell
			});

			const newCells: Cell[] = applyStyleToAGroupOfCells({
				cells,
				selectedGroup,
				style: 'highlight'
			});

			setCells([...newCells]);
		}
	}

	function mouseUpInCellAction(cell: Cell) {
		if (clickPressed && sourceCell) {
			const selectedGroup: CellRange = getSelectedCoordinatesCells({
				cell,
				sourceCell
			});

			const cellsUpdateData: CellsUpdateInfo = applyColorToAGroupOfCells({
				cells: [...cells],
				selectedGroup,
				color: sourceCell.color
			});

			if (cellsUpdateData) {
				setCells([...cellsUpdateData.all]);
				setCellsToNotify([ ...cellsToNotify, ...cellsUpdateData.selected ]);
			}

			setClickPressed(false);
		}
	}

	function onMouseLeaveBoard() {
		let copyCells: Cell[] =
			[...cells].map((cell: Cell): Cell => ({
				...cell,
				style: ''
			}));

		setCells(copyCells);
	}

	function initBoardData() {
		const newCells: Cell[] = generateCellsData(size);
		setCells(newCells);
	}

	function onMouseUpInCell($event: MouseEvent) {
		const cell: Cell | null = getCellDataFromEvent($event, cells);

		if (cell) mouseUpInCellAction(cell);

		setSourceCell(null);
		setClickPressed(false);
	}

	function onDoubleTouchInCell($event: TouchEvent) {
		const cell: Cell | null = getCellDataFromEvent($event, cells);
		if (cell) applyColorToColumn(cell.col, cell.color);
	}

	function touchAction($event: TouchEvent) {
		return setTimeout(() => {
			const cell: Cell | null = getCellDataFromEvent($event, cells);
			const isSourceCell = cell?.index === sourceCell?.index;

			if (cell && isSourceCell) {
				const newCells: Cell[] = toggleColor([...cells], cell.index);
				setCells(newCells);

				setCellsToNotify([ ...cellsToNotify, cell ]);
			}
			else if (cell && clickPressed && sourceCell) {
				const selectedGroup: CellRange = getSelectedCoordinatesCells({
					cell,
					sourceCell
				});

				const cellsUpdateData: CellsUpdateInfo = applyColorToAGroupOfCells({
					cells: [...cells],
					selectedGroup,
					color: sourceCell.color
				});

				if (cellsUpdateData) setCells([...cellsUpdateData.all]);
			}

			setClickPressed(false);
			clearTouchTimeout();
		}, TIME_OF_SINGLE_CLICK);
	}

	function onTouchStart($event: TouchEvent) {
		document.documentElement.style.overflow = 'hidden';

		const cell: Cell | null = getCellDataFromEvent($event, cells);
		setClickPressed(true);

		if (cell) setSourceCell(cell);
	}

	function onTouchEnd($event: TouchEvent) {
		document.documentElement.style.overflow = 'auto';

		setTouchs([
			...touchs,
			{
				time: new Date().getTime(),
				$event
			}
		]);

		setSourceCell(null);
		setClickPressed(false);
	}

	function onTouchMove($event: TouchEvent) {
		if (touchs.length > 0) setTouchs([]);
		if (touchTimeOutInstance) clearTouchTimeout();

		const cell: Cell | null = getCellDataFromEvent($event, cells);

		if (!sourceCell || !cell) return;

		const selectedGroup: CellRange = getSelectedCoordinatesCells({
			cell,
			sourceCell
		});

		const newCells: Cell[] = applyStyleToAGroupOfCells({
			cells,
			selectedGroup,
			style: 'highlight'
		});

		setCells([...newCells]);
	}

	// clicks controller (handle if is a single or double click)
	useEffect(() => {
		if (touchs.length === 1 && !touchTimeOutInstance) {
			const touchEvent: TouchEvent = touchs[0].$event;
			setTouchTimeOutInstance(touchAction(touchEvent));
		}

		if (touchs.length < 2) return;

		const [first, second] = touchs.slice(-2);
		const isDoubleTouch = (second.time - first.time) < TIME_OF_SINGLE_CLICK;

		if (isDoubleTouch) {
			clearTouchTimeout();
			const touchEvent: TouchEvent = touchs[0].$event;
			onDoubleTouchInCell(touchEvent);
		}
	}, [touchs]);


	const touchEventListeners = [
		{ name: 'touchmove', callback: throttle(onTouchMove, 100) },
		{ name: 'touchstart', callback: onTouchStart },
		{ name: 'touchend', callback: onTouchEnd }
	];
	const desktopEventListeners = [
		{ name: 'mousedown', callback: onMouseDownInCell },
		{ name: 'mouseover', callback: onMouseOverInCell },
		{ name: 'mouseup', callback: onMouseUpInCell },
		{ name: 'click', callback: onClickInCell }
	];

	function startListeners() {
		const listeners = isMobile ? touchEventListeners : desktopEventListeners;
		listeners.forEach(({ name, callback }: any) => {
			document.addEventListener(name, callback);
		});
	}

	function clearListeners() {
		const listeners = isMobile ? touchEventListeners : desktopEventListeners;
		listeners.forEach(({ name, callback }: any) => {
			document.removeEventListener(name, callback);
		});
	}

	useEffect(() => {
		startListeners();

		return () => {
			clearListeners();
		};
	}, [cells, touchs, sourceCell]);

	useEffect(() => {
		setSize(props.size);
	}, [props.size]);

	// render board on change size state
	useEffect(() => {
		setIsMobile(IS_MOBILE(navigator));
		initBoardData();
	}, [size, window.navigator]);

	useEffect(() => {
		if (cellsToNotify.length) {
			sendNotification();
		}

		return () => {
			clearTimeout(notificationTimeout);
		};
	}, [cellsToNotify]);

	return (<div
		ref={React.createRef()}
		className="Board"
		onMouseLeave={onMouseLeaveBoard}
	        >
		{size < 2 && <div>Please, size over 2</div>}
		{size > 1 && generateBoard(cells, size)}
		<div className="Reset"><button onClick={initBoardData}><div style={{ marginRight: '1rem' }}>reset</div> <ReloadIcon /></button></div>
	</div>
	);
}

export default Board;