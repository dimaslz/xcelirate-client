import { Cell, CellRange } from '../../../entities';

type Params = {
	cell: Cell;
	sourceCell: Cell;
}

export default function getSelectedCoordinatesCells({
	cell,
	sourceCell
}: Params): CellRange {
	const { row: cellRow, col: cellCol } = cell;

	let col = { from: 0, to: 0 };
	let row = { from: 0, to: 0 };

	// get COL from to
	if (cellCol > sourceCell.col) {
		col.from = +sourceCell.col;
		col.to = +cellCol;
	}
	else {
		col.from = +cellCol;
		col.to = +sourceCell.col;
	}

	// get ROW from to
	if (cellRow > sourceCell.row) {
		row.from = +sourceCell.row;
		row.to = +cellRow;
	}
	else {
		row.from = +cellRow;
		row.to = +sourceCell.row;
	}

	return { row, col };
}