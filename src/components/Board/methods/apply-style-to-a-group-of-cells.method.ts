import { Cell, CellRange, CellsUpdateInfo } from '../../../entities';

import { applyCallbackToCellsInAndOut } from './';

type Params = {
	cells: Cell[];
	selectedGroup: CellRange;
	style: string;
}

export default function applyStyleToAGroupOfCells(
	{
		cells,
		selectedGroup,
		style
	}: Params) {
	let { all }: CellsUpdateInfo = applyCallbackToCellsInAndOut({
		cells,
		selectedGroup,
		cbToCellInside: (cell) => ({ ...cell, style }),
		cbToCellOutside: (cell) => ({ ...cell, style: '' }) }
	);

	return all;
}