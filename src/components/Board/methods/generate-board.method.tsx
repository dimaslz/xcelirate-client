import React from 'react';

import { Cell } from '../../../entities';
import CellComponent from '../../Cell/Cell.component';

export default function generateBoard(cells: Cell[], size: number) {
	const cellsCopy: Cell[] = [...cells];

	return Array
		.from(new Array(size))
		.map((_, index) => {
			const rowCells = (cellsCopy || []).splice(0, size);

			return (
				<div key={index}
					className="Row"
					data-testid={`row_${index}`}
				>
					{rowCells.map((cell: Cell, cellIndex: number) => (
						<CellComponent
							key={cellIndex}
							cell={cell}
						/>
					))}
				</div>
			);
		});
}