export default function getColumnByCellIndex(cellIndex: number, size: number) {
	if (cellIndex > size - 1) {
		const row = Math.floor((cellIndex / size));
		const col = cellIndex - (size * row);

		return col;
	}

	return cellIndex;
}