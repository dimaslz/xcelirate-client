export { default as generateCellsData }  from './generate-cells-data.method';
export { default as toggleColor } from './toggle-color.method';
export { default as applyCallbackToCellsInAndOut } from './apply-callback-to-cells-in-and-out.method';
export { default as getSelectedCoordinatesCells } from './get-selected-coordinates-cells.method';
export { default as getCellDataFromEvent } from './get-cell-data-from-event.method';
export { default as generateBoard } from './generate-board.method';
export { default as applyStyleToAGroupOfCells } from './apply-style-to-a-group-of-cells.method';
export { default as applyColorToAGroupOfCells } from './apply-color-to-a-group-of-cells.method';