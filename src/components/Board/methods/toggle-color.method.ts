import { Cell } from '../../../entities';
import { COLORS } from '../../../enums';

export default function toggleColor(cells: Cell[], cellIndex: number): Cell[] {
	const cellsCopy = [...cells];
	const cell = cellsCopy[cellIndex];

	const color = cell.color === COLORS.RED ? COLORS.GREEN : COLORS.RED;
	cellsCopy[cellIndex] = {
		...cell,
		color
	};

	return cellsCopy;
}