import { Cell, CellRange, CellsUpdateInfo } from '../../../entities';

type Params = {
	cells: Cell[];
	selectedGroup: CellRange;
	cbToCellInside: (cell: Cell) => Cell;
	cbToCellOutside: (cell: Cell) => Cell;
}
export default function applyCallbackToCellsInAndOut({
	cells,
	selectedGroup,
	cbToCellInside,
	cbToCellOutside
}: Params): CellsUpdateInfo {
	let cellsInsideGroup: Cell[] = [];
	let cellsOutsideGroup: Cell[] = [];
	const { row, col } = selectedGroup;


	let cellsCopy: Cell[] = [...cells];
	cellsCopy = cellsCopy.map((cell: Cell) => {
		const colIsIn = col.from <= cell.col && cell.col <= col.to;
		const rowIsIn = row.from <= cell.row && cell.row <= row.to;
		const cellIsIn = colIsIn && rowIsIn;

		if (cellIsIn && cbToCellInside) { cell = cbToCellInside(cell); }
		if (!cellIsIn && cbToCellOutside) { cell = cbToCellOutside(cell); }
		(cellIsIn ? cellsInsideGroup : cellsOutsideGroup).push(cell);

		return cell;
	});

	return {
		selected: cellsInsideGroup,
		rest: cellsOutsideGroup,
		all: cellsCopy
	};
}