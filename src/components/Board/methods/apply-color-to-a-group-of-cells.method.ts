import { Cell, CellRange, CellsUpdateInfo } from '../../../entities';
import applyCallbackToCellsInAndOut from './apply-callback-to-cells-in-and-out.method';

type Params = {
	cells: Cell[];
	selectedGroup: CellRange;
	color: string;
}

export default function applyColorToAGroupOfCells({
	cells,
	selectedGroup,
	color
}: Params): CellsUpdateInfo {
	const { row, col } = selectedGroup;
	if (row.from !== row.to || col.from !== col.to) {
		return applyCallbackToCellsInAndOut({
			cells: [...cells],
			selectedGroup: { row, col },
			cbToCellInside: (cell) => ({ ...cell, style: '', color }),
			cbToCellOutside: (cell) => ({ ...cell, style: '' }) }
		);
	}

	return {
		all: cells,
		selected: [],
		rest: []
	};
}