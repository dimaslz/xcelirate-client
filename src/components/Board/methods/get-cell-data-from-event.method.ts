import { Cell } from '../../../entities';

export default function getCellDataFromEvent(
	$event: MouseEvent | TouchEvent | null,
	cells: Cell[]
): Cell | null {
	let target: HTMLElement | EventTarget | null = null;

	if ('TouchEvent' in window && $event instanceof TouchEvent) {
		target = document.elementFromPoint(
			$event.changedTouches[0].pageX,
			$event.changedTouches[0].pageY
		);
	}
	else if ($event instanceof MouseEvent) {
		target = $event?.target;
	}

	if (!target) return null;

	if (target instanceof HTMLElement) {
		let cellIndex: string | undefined = target.dataset.index;
		return cellIndex ? [...cells][parseInt(cellIndex, 10)] : null;
	}

	return null;
}