import { Cell } from '../../../entities';
import { COLORS } from '../../../enums';

export default function generateCellsData(size: number): Cell[] {
	let col = -1;
	let row = -1;

	const cellsArr = Array
		.from(new Array(size * size));

	return cellsArr
		.map((_, index) => {
			col < size - 1 ? ++col : col = 0;
			if (col === 0) row < size-1 ? ++row : row = 0;

			return {
				col,
				row,
				index,
				style: '',
				color: COLORS.GREEN
			};
		});
}