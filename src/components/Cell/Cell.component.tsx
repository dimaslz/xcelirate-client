import React from 'react';
import { Cell } from '../../entities';

import './Cell.component.scss';

type Props = {
	cell: Cell;
}

function CellComponent({ cell }: Props) {
	return (<div
		role="cell"
		data-testid={`cell_${cell.index}`}
		data-index={`${cell.index}`}
		className={[
			'Cell',
			`Cell__${cell.color}`,
			cell.style
		].join(' ')}
	        />);
}

export default React.memo(CellComponent);