# Challenge notes
As an addition, this project have a linter for commits to try to follow good practices as [https://www.conventionalcommits.org/en/v1.0.0/](https://www.conventionalcommits.org/en/v1.0.0/) with `@commitlint/cli` and husky (there area a postinstall hook to install it)

## Initial comment
This is my solution to get the requirements requested but, is not the final. I did some updates and refactors because, every time I check the code, I considered that I can improve it but, I decided to implement some ideas and try to write with which ideas or decisions I would like to continue.

Between lines of this document, there are some **[QUESTION]** that's means that is a feature that could have different implementation talking more in detail or depends of the objective of the "product".

## Required features implemented
- [x] On click in a cell, will toggle between RED and GREEN color
- [x] Double-click a cell to set the whole column to the same color as the cell
- [x] On select a group of cells, should apply the color of the source cell
- [x] One changed a cell color, if there was not change in 1 second, should send information to `https://postman-echo.com/post` by POST
  - Here, I decided to request to this endpoint by proxy by the path `track` because I understood that I need to simulate to send the activity to some tracking service, for example.
  - Example: If you click several cells (3 in this case) every 500ms, after 1 second, is sending a request to `http://localhost:8082/track` with data `{ cells: [{...cellInfo}, ...] }`
- [x] Once the change is done, show a notification in the screen. If there is an error, will throw a notification error
  - **[QUESTION]** Should be change the color of cell? or should back to the initial state?
- [ ] Should be tested and work in mobile
  - Honestly, I tried to finish this feature, but finally I didn't it. I was trying to work with some event like `pointer{over, move, up...}` or `touch{start, up, move...}`, but I decided to send this version, tested in Firefox, Chrome and Safari, and still continue looking the best way to do it, at least, as a personal challenge.

## Extra features
- [x] To avoid reload the page, there are a `RESET` button to reset the board

## Technical comments
* `App.tsx`: I was thinking about to create an abstraction of this behavior but I decided to implement the notifications as a simple component Notification.component, with the unique responsibility to print the notification in top or bottom of the screen
```javascript
// notification example
{
	message: 'My notification message',
	type: 'success', // by default is 'success' but, also can be 'error'
	position: 'top', // by default is 'top' but, also can be 'bottom'
}
```
* `Board.component.tsx`: This is the responsible to render the board, according with the size passed by property. By default is `DEFAULT_SIZE` value.
	- Control events by events from document to don't have a listener per each Cell component (`line 188 to 213`)
	- Control if is a single click or double click (`from line 170 to 186`). I have considered that is a DoubleClick if is coming two clicks in less than `TIME_OF_SINGLE_CLICK` value. **WHY?**: If I use `onDoubleClick` event, `onClick` is working at the same time, so, just with the `onClick` event, I control if is a click or doubleClick.
  	- Possible refactor: Create a hook to handle the click events. With this implementation, we can move more code to an independent file with specific responsibility
	- I tried to move to methods any method and just leave the method with event responsibility.

* `sdk` folder: This project is like a library SDK of the API. The intention is simulate a library that is the SDK of our API, in this case with the endpoint POST to postman-echo.com/post. To ovoid problems on install and I copy the dist result of this project and paste in "`@xcelirate/sdk`" folder. This should be in `node_modules/@xcelirate/sdk` for example.