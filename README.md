# Xcelirate challenge
To get information about the project decisions of points of view, please read this [./CHALLENGE_NOTES.md](./CHALLENGE_NOTES.md)

## Stack
* React (`.tsx`)
* Typescript
* SASS
* Testing Library

I started this project with [Create React App](https://github.com/facebook/create-react-app).

## Before run this project, should run the proxy to make the request to POSTMAN
**Why?**: To use this Postman demo service [https://postman-echo.com/post](https://postman-echo.com/post), is not allowed to make request from client side, so, we need to use a proxy. Then, to use the proxy, follow the next steps.

**(*)** I used Yarn for this project, so, I recommend to use it.

* In a terminal, go to folder `/proxy`
* `yarn install`
* `yarn build`
* `yarn start`
* an express service is running in port `8082` [http://localhost:8082](http://localhost:8082)
* now, let's run the client side project (this project)

## How to run
* In a terminal, go to folder `/client`
* `yarn install`
* `yarn start`
* Now will open the port `3000` in [http://localhost:3000](http://localhost:3000)
* If you want to use the compiled version, run `yarn build` and serve the `build/index.html`, with Mac (OSX), I use `php -S localhost:3000 -t build`

## Unit Tests
We use [https://testing-library.com/](https://testing-library.com/). The principal focus of this library, is test as a user, how is render de component after different behaviors.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

## Author
Dimas López · FullStack Software Developer

🐦 https://twitter.com/dimaslz

👨🏻‍💻 https://dimaslz.dev

📄 https://www.linkedin.com/in/dimaslopezzurita